// References:
// https://blog.logrocket.com/how-to-use-flutter-hooks/
// https://medium.com/flutter-community/flutter-hooks-7754df814995

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:flutter_calibrar/characters/data/models/character.dart';
import 'package:flutter_calibrar/common/utils/responsive_layout.dart';

class RMCharactersList extends HookWidget {
  final Character character;

  const RMCharactersList({
    Key? key,
    required this.character,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final responsive = Responsive(context);
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth < 600 || responsive.isMobileView) {
        // Mobile view - List Layout
        return buildListLayout();
      } else {
        // Web view - Grid Layout
        return buildGridLayout();
      }
    });
  }

  Widget buildListLayout() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      width: double.infinity,
      child: Center(
        child: Column(
          children: [
            Container(
              width: 150.0,
              height: 150.0,
              child: Image.network(character.image ?? ''),
            ),
            SizedBox(height: 10.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildCharacterName(),
                  buildGenderStatusRow(),
                  buildSpeciesTypeRow(),
                  SizedBox(height: 12.0),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildGridLayout() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 150,
            height: 150,
            child: Image.network(character.image ?? ''),
          ),
          SizedBox(width: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCharacterName(),
                buildGenderStatusRow(),
                buildSpeciesTypeRow(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildCharacterName() {
    return Container(
      alignment: Alignment.center,
      child: Text(
        character.name ?? '',
        style: const TextStyle(
            fontFamily: 'RockSalt',
            fontWeight: FontWeight.bold,
            fontSize: 17,
            color: Colors.lightGreen),
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildGenderStatusRow() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          character.gender ?? '',
          style: const TextStyle(
            fontFamily: 'Montserrat-Medium',
            fontSize: 12,
            color: Colors.lightBlue,
          ),
        ),
        SizedBox(width: 8),
        const Text(
          '||',
          style: TextStyle(
            color: Colors.lime,
          ),
        ),
        SizedBox(width: 8),
        Text(
          character.status ?? '',
          style: const TextStyle(
            fontFamily: 'Montserrat-Medium',
            fontSize: 12,
            color: Colors.lightBlue,
          ),
        ),
      ],
    );
  }

  Widget buildSpeciesTypeRow() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          character.species ?? 'Unknown',
          style: const TextStyle(
            fontFamily: 'Montserrat-Medium',
            fontSize: 10,
            color: Colors.amber,
          ),
        ),
      ],
    );
  }
}
