// Reference:
// https://rickandmortyapi.com/documentation/#get-all-characters

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:flutter_calibrar/common/components/nav_bar.dart';
import 'package:flutter_calibrar/characters/data/providers/characters_provider.dart';
import 'package:flutter_calibrar/characters/presentation/modules/components/character_list.dart';
import 'package:flutter_calibrar/common/utils/responsive_layout.dart';

var rmCharacters = [];
var i = 0;

class CharacterPage extends HookConsumerWidget {
  const CharacterPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _isLoading = useState(false);
    final _graphqlProvider = ref.watch(graphqlProvider);
    final _completeLoading = useState(false);
    final scrollController = useScrollController();
    final responsive = Responsive(context);

    _loadData() async {
      _isLoading.value = true;

      i++;
      await _graphqlProvider.getCharacters(i);
      var ramData =
          rmCharacters.length >= 826 ? [] : _graphqlProvider.characters;
      if (ramData.isNotEmpty) {
        rmCharacters.addAll(ramData);
      }

      _isLoading.value = false;
      _completeLoading.value = ramData.isEmpty;
    }

    useEffect(() {
      _loadData();
      scrollController.addListener(() {
        if (scrollController.position.pixels >=
                scrollController.position.maxScrollExtent &&
            !_isLoading.value) {
          _loadData();
        }
      });
      return () => scrollController.removeListener;
    }, [scrollController]);

    return Scaffold(
      appBar: CustomNavBar(),
      backgroundColor: Colors.black,
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: LayoutBuilder(builder: (context, constraints) {
          if (responsive.isMobileView || constraints.maxWidth < 600) {
            // Mobile view - List Layout
            if (rmCharacters.isNotEmpty) {
              return ListView.separated(
                controller: scrollController,
                itemCount: rmCharacters.length,
                separatorBuilder: (context, index) => SizedBox(height: 8.0),
                itemBuilder: (context, index) {
                  final character = rmCharacters[index];
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Container(
                      color: Colors.grey[600],
                      child: RMCharactersList(
                        character: character,
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.amber)));
            }
          } else {
            // Web view - Grid Layout
            if (rmCharacters.isNotEmpty) {
              return GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
                controller: scrollController,
                itemCount: rmCharacters.length,
                itemBuilder: (context, index) {
                  final character = rmCharacters[index];
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Container(
                      color: Colors.grey[600],
                      child: RMCharactersList(
                        character: character,
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.amber)));
            }
          }
        }),
      ),
    );
  }
}
