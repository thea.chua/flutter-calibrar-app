import 'package:graphql/client.dart';

final _baseURL = HttpLink('https://rickandmortyapi.com/graphql/');

final graphQlClient = GraphQLClient(
    link: _baseURL,
    cache: GraphQLCache(),
    defaultPolicies: DefaultPolicies(
        query: Policies.safe(FetchPolicy.networkOnly, ErrorPolicy.none,
            CacheRereadPolicy.mergeOptimistic)));
