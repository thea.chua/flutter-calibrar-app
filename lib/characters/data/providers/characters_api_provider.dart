import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:flutter_calibrar/characters/data/api/characters_graphql.dart';

final graphqlApiProvider =
    Provider<GraphqlApiProvider>((ref) => GraphqlApiProvider());

class GraphqlApiProvider {
  final characters = CharactersGraphQL();
}
