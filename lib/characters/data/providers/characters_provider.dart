import 'package:flutter/cupertino.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:flutter_calibrar/characters/data/api/characters_graphql.dart';
import 'package:flutter_calibrar/characters/data/models/character.dart';
import 'package:flutter_calibrar/characters/data/providers/characters_api_provider.dart';

final graphqlProvider = ChangeNotifierProvider((ref) {
  final charactersGraphQL = ref.watch(graphqlApiProvider).characters;
  return CharactersNotifier(charactersApi: charactersGraphQL);
});

class CharactersNotifier extends ChangeNotifier {
  final CharactersGraphQL? charactersApi;

  CharactersNotifier({this.charactersApi});

  List<Character> _characters = [];
  List<Character> get characters => _characters;

  Future<List<Character>> getCharacters(i) async {
    final data = await charactersApi!.getCharacters(i);
    _characters = data;
    return _characters;
  }
}
