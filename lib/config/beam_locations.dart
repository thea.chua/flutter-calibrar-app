import 'package:flutter/material.dart';
import 'package:beamer/beamer.dart';
import 'package:flutter_calibrar/blog/presentation/modules/screens/blog.dart';

import 'package:flutter_calibrar/characters/presentation/modules/screens/character_home.dart';

class RouteLocationGenerator {
  static const String homeRoute = '/';
  static const String blogRoute = '/blog';
  static const String rmRoute = '/characters';
}

class SimpleLocationGenerator {
  static final simpleLocationBuilder = RoutesLocationBuilder(routes: {
    '/': (context, state, data) => BeamPage(
        key: ValueKey('blog'),
        title: 'Blog',
        child: BlogPage(),
        type: BeamPageType.noTransition),
    '/blog': (context, state, data) => BeamPage(
        key: ValueKey('blog'),
        title: 'Blog',
        child: BlogPage(),
        type: BeamPageType.noTransition),
    '/characters': (context, state, data) => const BeamPage(
        key: ValueKey('characters'),
        title: 'Characters',
        child: CharacterPage(),
        type: BeamPageType.noTransition),
  });
}
