import 'package:flutter_calibrar/blog/data/api/blog_remote_source.dart';
import 'package:flutter_calibrar/blog/data/repository/blog_repository.dart';

final blogRemoteSource = BlogRemoteSource();
final blogRepository = BlogRepository(blogRemoteSource);
