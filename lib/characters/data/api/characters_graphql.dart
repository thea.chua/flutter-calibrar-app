// Reference:
// https://rickandmortyapi.com/documentation/#get-all-characters

import 'package:graphql/client.dart';
import 'package:flutter_calibrar/characters/data/models/character.dart';
import 'package:flutter_calibrar/config/characters_remote_source.dart';

class CharactersGraphQL {
  String _getCharacters = r'''
  query GetCharacters($page: Int) {
    characters(page: $page) {
      info {
        count
        pages
        next 
        prev
      }
      results {
        id
        name
        status
        species
        type
        gender
        image
      }
    }
  }
  ''';

  Future<List<Character>> getCharacters(i) async {
    final options = QueryOptions(document: gql(_getCharacters), variables: {
      'page': i,
    });

    final response = await graphQlClient.query(options);

    if (!response.hasException) {
      final List<Object?> data = response.data!['characters']['results'];
      final values = data.map(
          (character) => Character.fromJson(character as Map<String, dynamic>));
      return values.toList();
    } else {
      return [];
    }
  }
}
