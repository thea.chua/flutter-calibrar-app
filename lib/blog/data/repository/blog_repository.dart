import 'dart:async';

import 'package:flutter_calibrar/blog/data/repository/blog_repository_interface.dart';
import 'package:flutter_calibrar/blog/data/api/blog_remote_source.dart';
import 'package:flutter_calibrar/blog/data/models/summary_form.dart';
import 'package:flutter_calibrar/blog/data/models/summary_entry.dart';

class BlogRepository implements BlogRepositoryInterface {
  final BlogRemoteSource _blogRemoteSource;

  BlogRepository(this._blogRemoteSource);

  @override
  Future<SummaryEntry> addEntry(SummaryForm value) async {
    return await _blogRemoteSource.addEntry(value);
  }

  @override
  Future<List<SummaryEntry>> deleteEntry(String id) async {
    return await _blogRemoteSource.deleteEntry(id);
  }

  @override
  Future<List<SummaryEntry>> retrieveSummaries() async {
    return await _blogRemoteSource.retrieveSummaries();
  }
}
