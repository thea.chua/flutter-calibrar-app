class Character {
  String? _id;
  String? _image;
  String? _name;
  String? _status;
  String? _species;
  String? _type;
  String? _gender;

  Character({
    String? id,
    String? image,
    String? name,
    String? status,
    String? species,
    String? type,
    String? gender,
  }) {
    _id = id;
    _image = image;
    _name = name;
    _status = status;
    _species = species;
    _type = type;
    _gender = gender;
  }

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
      id: json['id'] as String? ?? '',
      image: json['image'] as String? ?? '--',
      name: json['name'] as String? ?? '',
      status: json['status'] as String? ?? '',
      species: json['species'] as String? ?? '',
      type: json['type'] as String? ?? '',
      gender: json['gender'] as String? ?? '',
    );
  }

  String? get id => _id;
  String? get image => _image;
  String? get name => _name;
  String? get status => _status;
  String? get species => _species;
  String? get type => _type;
  String? get gender => _gender;
}
